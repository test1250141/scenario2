

FROM alpine:latest

RUN apk update && apk add --no-cache nginx vim npm

WORKDIR /app
COPY package.json app.js ./

RUN npm install

COPY web /usr/share/nginx/html
COPY default.conf /etc/nginx/http.d/

EXPOSE 80 3333

CMD ["sh", "-c", "nginx -g 'daemon off;' & node app.js"]


#docker build -t app-s2-image .  
#docker run -d -p 80:80 -p 3333:3333 app-s2-image
