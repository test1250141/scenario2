
let data = 0;
const countingElement = document.getElementById("counting");

const baseUrl = 'http://localhost:30033';

function getValue() {
    fetch(`${baseUrl}/api/get/value`)
        .then(response => response.json())
        .then(data => {
            document.getElementById('envValue').innerText = data.myVal;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

function getCounterValue() {
    fetch(`${baseUrl}/api/get`)
        .then(response => response.json())
        .then(data => {
            data = data.counter;
            countingElement.innerText = data;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

function increment() {
    fetch(`${baseUrl}/api/incr`, {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json', 
        },
        body: JSON.stringify({}), 
    })
        .then(response => response.json())
        .then(data => {
            data = data.counter;
            countingElement.innerText = data;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

function decrement() {
    fetch(`${baseUrl}/api/decr`, {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json', 
        },
        body: JSON.stringify({}), 
    })
        .then(response => response.json())
        .then(data => {
            data = data.counter;
            countingElement.innerText = data;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}
getValue();
document.addEventListener('DOMContentLoaded', getCounterValue);
document.getElementById('increment').addEventListener('click', increment);
document.getElementById('decrement').addEventListener('click', decrement);
