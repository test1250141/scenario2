# Prereq

This sample application is tested only on Kubernetes for Docker Desktop on linux containers on Windows AMD64. Other Kubernetes alternative might not reproduce the application exactly due to how the browser calls the backend api.

- kubectl config get-contexts
- kubectl config use-context docker-desktop

# Running the application
kubectl apply -f db.yml

kubectl apply -f app.yml 
>requires mysql to finish initializing and ready for connection,**kubectl logs mysql-pod-name** or else app will fail to connect to db and the app will not retry; therefore terminating the pod is required. **kubectl delete pod app-pod-name** 

kubectl get svc nginx
>copy the node port value

Visit http://localhost:node-port

## Start Prometheus and Grafana

Download helm
>https://helm.sh/docs/intro/install/

Add Prometheus
>helm repo add prometheus-community [https://prometheus-community.github.io/helm-charts](https://prometheus-community.github.io/helm-charts)

Add Grafana
>helm repo add grafana [https://grafana.github.io/helm-charts](https://grafana.github.io/helm-charts)

 >helm repo update
 
> helm install prometheus-release prometheus-community/prometheus

>helm install grafana-release grafana/grafana

>kubectl expose service prometheus-release-server --type=NodePort --target-port=9090 --name=prometheus-server-ext --port=9090

>kubectl get nodes -o wide

>kubectl get svc prometheus-server-ext

Copy the node-ip and node port for prometheus-server-ext service
   
### Setup Grafana
- kubectl get secret grafana-release -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
   > Copy password for login to Grafana, user is admin

- export POD_NAME=$(kubectl get pods -l app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana-release" -o jsonpath="{.items[0].metadata.name}")

- kubectl port-forward $POD_NAME 3000
- Login grafana localhost:3000 -> Add datasource -> Select prometheus -> Set Prometheus server URL to http://node-ip:node-port -> Save & Test

### Prometheus
- export POD_NAME=$(kubectl get pods -l "app.kubernetes.io/name=prometheus-pushgateway,app.kubernetes.io/instance=prometheus-release" -o jsonpath="{.items[0].metadata.name}")

- kubectl port-forward $POD_NAME 9091
- localhost:9001


## Building for docker

> cd db
- docker build -t db-image-name .
- docker run -d -p 3306:3306 db-image-name
##### get IP of docker container running the db 
> docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id
##### Replace variables 
> `mysqlHost`in app.js to the ip returned from above
>
> `baseUrl` in web/js/s2.js to `'http://localhost:3333'`
- docker build -t app-image-name .
- docker run -d -p 80:80 -p 3333:3333 app-image-name

>docker login

>docker tag s2-image:latest dockerhub-username/scenario2:v1.0.0

>docker push dockerhub-username/scenario2:v1.0.0
