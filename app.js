const express = require('express');
const mysql = require('mysql2');
const app = express();
const port = 3333;
const cors = require('cors');
app.use(cors())
const mysqlHost = process.env.MYSQL_HOST || '172.17.0.2';
const myVal = process.env.MY_VAL || 'val';

console.log(mysqlHost);
console.log(myVal);

const connection = mysql.createConnection({
          host: mysqlHost,
          user: 'root',
          password: 'passwd',
          database: 'mydb',
});

connection.connect(error => {
  if (error) {
    console.error('Error connecting to the database:', error);
  } else {
    console.log('Connected to the database');
  }
});

app.get('/api/get/value', (req, res) => {
    res.json({ myVal });
});


function retrieveCounterValue(callback) {
  connection.query('SELECT counter_value FROM my_counter', (error, results) => {
    if (error) {
      console.error('Error retrieving counter value:', error);
      callback(error, null);
    } else {
      const counter = results[0].counter_value;
      callback(null, counter);
    }
  });
}

app.get('/api/get', (req, res) => {

  retrieveCounterValue((error, counter) => {
    if (error) {
      res.status(500).json({ error: 'Internal server error' });
    } else {
      res.json({ counter });
    }
  });
});

app.post('/api/incr', (req, res) => {
  connection.query('UPDATE my_counter SET counter_value = counter_value + 1', (error, results) => {
    if (error) {
      console.error('Error incrementing counter:', error);
      res.status(500).json({ error: 'Internal server error' });
    } else {
      
      retrieveCounterValue((error, counter) => {
        if (error) {
          res.status(500).json({ error: 'Internal server error' });
        } else {
          res.json({ message: 'Counter incremented successfully', counter });
        }
      });
    }
  });
});

app.post('/api/decr', (req, res) => {
  connection.query('UPDATE my_counter SET counter_value = counter_value - 1', (error, results) => {
    if (error) {
      console.error('Error incrementing counter:', error);
      res.status(500).json({ error: 'Internal server error' });
    } else {
      
      retrieveCounterValue((error, counter) => {
        if (error) {
          res.status(500).json({ error: 'Internal server error' });
        } else {
          res.json({ message: 'Counter incremented successfully', counter });
        }
      });
    }
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
