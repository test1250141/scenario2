
USE mydb;
CREATE TABLE my_counter (id INT AUTO_INCREMENT PRIMARY KEY, counter_value INT DEFAULT 0);

DELIMITER //

CREATE PROCEDURE RunQueryIfTableEmpty()
BEGIN
    DECLARE rowCount INT;
    -- Check the row count of the table
    SELECT COUNT(*) INTO rowCount FROM my_counter;
    IF rowCount = 0 THEN
        -- Table is empty, execute your query here
		INSERT INTO my_counter (counter_value) VALUES (0);
    END IF;
END //

DELIMITER ;

CALL RunQueryIfTableEmpty();
